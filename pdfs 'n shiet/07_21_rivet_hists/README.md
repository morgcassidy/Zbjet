# A short note for these distributions:
 
The rivet analysis used is ATLAS_2020_I1788444. The 1e5 tag attached to each histogram indicates that pythia was run with 1e5 events. For all attached histograms, the simulated data with pythia falls short in magnitude compared to "Data." I didn't know if that had anything to do with the number of events.
