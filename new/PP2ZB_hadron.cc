#include "Rivet/Analysis.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Tools/Cuts.hh"

#include "fastjet/contrib/SoftDrop.hh"
#include "fastjet/JadePlugin.hh"

// Standard C++ lib
#include <vector>
#include <sstream> // Need by Angularity

using namespace fastjet;
//---------------------------------------------------------------------------------------------------------------------
namespace Rivet 
{

    
// @ Morgan: I changed the constructor a bit
// namely now it is not a 'default constructor' meaning that it takes two arguments: beta and z_cut
// it makes a class JetFlavor more flexible (works with any pair of SD parameters)
// if you want to lear more about it read about "C++ default member initialization and constructors"
// it is a very useful trick! 
class JetFlavor
{
public:
    // SD parameters:
    // commmon values of beta = 0., 1., 2.
    // common values of z_cut = 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4

    JetFlavor(double beta, double z_cut) : m_sd(beta, z_cut, std::numeric_limits<double>::infinity())
    {
        m_plugin = new fastjet::JadePlugin();

        fastjet::JetDefinition m_jet_def = fastjet::JetDefinition(m_plugin);

        m_jet_def.delete_plugin_when_unused();
       
        m_recluster = { m_jet_def };

        m_sd.set_reclustering(true, &m_recluster);
    }

protected:
    fastjet::contrib::SoftDrop m_sd;

    JetDefinition::Plugin *m_plugin;

    fastjet::Recluster m_recluster;

public:
    PseudoJet groomer(const PseudoJet &jet)
    {
        PseudoJet sdj_jet = m_sd(jet);
        
        return sdj_jet;
    }
    
};


/// @ Morgan it is an class to compute JetAngularities from our old project
/// Please take a look at it

/// \class Angularity
/// definition of angularity
/// by Gregory Soyez 
class Angularity : public FunctionOfPseudoJet<double>{
public:
  /// ctor
  Angularity(double alpha, double jet_radius, double kappa=1.0, Selector constitCut=SelectorPtMin(0.)) : _alpha(alpha), _radius(jet_radius), _kappa(kappa), _constitCut(constitCut) {}

  /// description
  std::string description() const{
    std::ostringstream oss;
    oss << "Angularity with alpha=" << _alpha;
    return oss.str();
  }

  /// computation of the angularity itself
  double result(const PseudoJet &jet) const{
    // get the jet constituents
    vector<PseudoJet> constits = jet.constituents();

    // get the reference axis
    PseudoJet reference_axis = _get_reference_axis(jet);

    // do the actual coputation
    double numerator = 0.0, denominator = 0.0;
    unsigned int num = 0;
    for (const auto &c : constits){
      if (!_constitCut.pass(c)) continue;
      double pt = c.pt();
      // Note: better compute (dist^2)^(alpha/2) to avoid an extra square root
      numerator   += pow(pt, _kappa) * pow(c.squared_distance(reference_axis), 0.5*_alpha);
      denominator += pt;
      num += 1;
    }
    if (denominator == 0) return -1;
    // the formula is only correct for the the typical angularities which satisfy either kappa==1 or alpha==0.
    else return numerator/(pow(denominator, _kappa)*pow(_radius, _alpha));
  }

protected:
  PseudoJet _get_reference_axis(const PseudoJet &jet) const{
    if (_alpha>1) return jet;

    Recluster recluster(JetDefinition(antikt_algorithm, JetDefinition::max_allowable_R, WTA_pt_scheme));
    return recluster(jet);
  }

  double _alpha, _radius, _kappa;
  Selector _constitCut;
};


// @ Morgan: note that now I do not inherit from JetFlavor
// instead I use it in the same way as you use the SD
class PP2ZB_hadron : public Analysis
{
public:
    // SD parameters of the groomers _sd(beta, zc), _jf(beta, zc)
    // should be specified here!
    PP2ZB_hadron() : Analysis("PP2ZB_hadron"), _sd(0.5, 0.01), _jf(0.5, 0.01) {}
    
    // Leading jet parameters
    const double _jetR = 0.4, _jet_rap = 2.5;
 
    // define bins in log10 space for jet angularities
    int _nbins = 20;
    double _ledge = -4., _redge = 0.;

    void init () 
    {
        // Initialise and register projections
        FinalState fs( Cuts::abseta < 4.5 );
        
        // for the muons
        double mu_pt = 27.0;
        double mz_min = (91-15);
        double mz_max = (91+15);
        double mu_eta_max = 2.5;
    
        // for histos
        int bins = 20;
        int bins_range_low = 300;
        int bins_range_high = 1500;

        ZFinder zfinder(fs,Cuts::pT > mu_pt*GeV && Cuts::abseta < mu_eta_max, PID::MUON, mz_min*GeV, mz_max*GeV);
        declare(zfinder, "zfinder");

        // exclude leptons from jets
        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
        jet_input.addVetoOnThisFinalState(zfinder);
        declare(jet_input, "jet_input");
        
        //fastjet projection
        FastJets jets(jet_input, FastJets::ANTIKT, _jetR);
        declare(jets, "jets");
     
        // Set weights to zero
        _w_cut_bjet = 0.;
        _w_cut_qcd  = 0.;
   
        // Book histograms:
        // b-jet 
        book( _h_bpT,     "bpT",     bins, bins_range_low, bins_range_high); 
        book( _h_bpT_sd,  "bpT_sd",  bins, bins_range_low, bins_range_high); 
        book( _h_bpT_sdj, "bpT_sdj", bins, bins_range_low, bins_range_high); 
 
        // QCD jet 
        book( _h_bpT_qcd,     "bpT_QCD",     bins, bins_range_low, bins_range_high); 
        book( _h_bpT_sd_qcd,  "bpT_sd_QCD",  bins, bins_range_low, bins_range_high); 
        book( _h_bpT_sdj_qcd, "bpT_sdj_QCD", bins, bins_range_low, bins_range_high); 
        
        // \alpha = 0.5, so called LHA
        std::vector<std::string> name = {"ang", "ang_sd", "ang_fj"};
 
        // It is a vector of pointers
        // so we need to resize it before filling in!
        _h_ang05.resize( name.size() );
        _h_ang10.resize( name.size() );
        _h_ang20.resize( name.size() );

        _h_ang05_qcd.resize( name.size() );
        _h_ang10_qcd.resize( name.size() );
        _h_ang20_qcd.resize( name.size() );

        for (size_t i = 0; i < name.size(); ++i) 
        {
             book(_h_ang05[i] , name[i] + "_05",  linspace(_nbins, _ledge, _redge));
             book(_h_ang10[i] , name[i] + "_10",  linspace(_nbins, _ledge, _redge));
             book(_h_ang20[i] , name[i] + "_20",  linspace(_nbins, _ledge, _redge));
             
             book(_h_ang05_qcd[i] , name[i] + "_QCD_05",  linspace(_nbins, _ledge, _redge));
             book(_h_ang10_qcd[i] , name[i] + "_QCD_10",  linspace(_nbins, _ledge, _redge));
             book(_h_ang20_qcd[i] , name[i] + "_QCD_20",  linspace(_nbins, _ledge, _redge));
        }

        // this one is just to store the x-section value
        book(_h_sigma_qcd,  "xsec_cut_qcd",   linspace(1, -0.5, 0.5) );
        book(_h_sigma_bjet, "xsec_cut_bjet",  linspace(1, -0.5, 0.5) );
    }
        
    void analyze(const Event &event) 
    {
        const double w = event.weight();
        
        // Reconstruct Z-boson
        const ZFinder &zfinder = apply<ZFinder>(event, "zfinder");   
        
        if (zfinder.bosons().size() < 1) vetoEvent;	
        
        const Particles& mus = zfinder.constituents();

        // for jets 	
        Jets jets = apply<FastJets>(event, "jets").jetsByPt(Cuts::pT>400*GeV && Cuts::absrap < 2.5);
        idiscardIfAnyDeltaRLess(jets, mus, 0.4);
 
	if (jets.size() < 1) vetoEvent;
        
        // fill histograms for the QCD-jets
        fill_all(jets, _h_bpT_qcd, _h_bpT_sd_qcd, _h_bpT_sdj_qcd, _h_ang05_qcd, _h_ang10_qcd, _h_ang20_qcd);

        _w_cut_qcd += w;
        
        // Standard b-tagging
        Jets bjets = filter_select(jets, hasBTag( Cuts::pT > 5*GeV ));
        
        if ( bjets.size() < 1 )	vetoEvent;
     
        _w_cut_bjet += w;

        // Define SoftDrop
        // contrib::SoftDrop sd(_beta, _zc);
        // @ Morgan if you create and initialize your groomer in such a way
        // then it will be created and deleted any time you invoke the analyze() function
        // therefore SD will be created N-times (where N is a number of events)!
        // However, the dafault initialization makes everything more efficient 
        // (see the PP2ZB_hadron() constructor)
        
        // fill histograms for the b-jets
        fill_all(bjets, _h_bpT, _h_bpT_sd, _h_bpT_sdj, _h_ang05, _h_ang10, _h_ang20);
    }
            
    void finalize() 
    { 
        // Compute cross section (in pb) after the cuts
        double sigma_tot = crossSection() / picobarn;
        
        double weight_tot = sumOfWeights();
     
        // x-section after the cuts	  
        _h_sigma_bjet->fill(0., _w_cut_bjet * sigma_tot / weight_tot);
        
        _h_sigma_qcd->fill(0., _w_cut_qcd * sigma_tot / weight_tot);
    }

    // @ Morgan:
    // please not that here we plot log10(\lambda) not \lambda! 
    // A helper function to compute angularities
    // if \lambda = 0 we put it in the very first bin (which should be located at -inf)
    double _get_ang(double alpha, const PseudoJet &jet, double left_edge) 
    {
        double res = 0.;
            
        Angularity angularity(alpha, _jetR);
                
        double lambda_tmp = angularity(jet);
        
        if (lambda_tmp == 0.) res = 0.99 * left_edge;
        else                  res = std::log10(lambda_tmp);
            
        return res;
    }
    
    void fill_all(const Jets &jets, 
                  Histo1DPtr h,
                  Histo1DPtr h_sd,
                  Histo1DPtr h_jf, 
                  std::vector<Histo1DPtr> h05,
                  std::vector<Histo1DPtr> h10,
                  std::vector<Histo1DPtr> h20)
    {
        //No grooming
        h->fill(jets[0].perp());

        PseudoJet sd_leading = _sd(jets[0]);
        
        h_sd->fill(sd_leading.perp());

        PseudoJet sdj_leading = _jf.groomer(jets[0]);
        
        h_jf->fill(sdj_leading.perp());
        
        // LHA
        h05[0]->fill(_get_ang(0.5,  jets[0],     _ledge));
        h05[1]->fill(_get_ang(0.5,  sd_leading,  _ledge));
        h05[2]->fill(_get_ang(0.5,  sdj_leading, _ledge));

        // JetWidth
        h10[0]->fill(_get_ang(1.0,  jets[0],     _ledge));
        h10[1]->fill(_get_ang(1.0,  sd_leading,  _ledge));
        h10[2]->fill(_get_ang(1.0,  sdj_leading, _ledge));
        
        
        // JetThrust
        h20[0]->fill(_get_ang(2.0,  jets[0],     _ledge));
        h20[1]->fill(_get_ang(2.0,  sd_leading,  _ledge));
        h20[2]->fill(_get_ang(2.0,  sdj_leading, _ledge));
    }
            
private: 
    // pT-distributions; b-jets
    Histo1DPtr _h_bpT, _h_bpT_sd, _h_bpT_sdj;
    
    // pT-distributions; qcd-jets
    Histo1DPtr _h_bpT_qcd, _h_bpT_sd_qcd, _h_bpT_sdj_qcd;
    
    // x-section after cuts
    Histo1DPtr _h_sigma_bjet, _h_sigma_qcd;

    
    // @Morgan
    // Array of angularities
    // each vector would contained three angularities: 
    // [0] = ungroomed, [1] = standard SD, [2] = jet falvor
    std::vector<Histo1DPtr> _h_ang05, _h_ang05_qcd;
    std::vector<Histo1DPtr> _h_ang10, _h_ang10_qcd;
    std::vector<Histo1DPtr> _h_ang20, _h_ang20_qcd;

    // use to get cross section after the cuts
    double _w_cut_bjet, _w_cut_qcd;
    
    // groomers
    contrib::SoftDrop _sd;
    JetFlavor _jf;
};
    
    DECLARE_RIVET_PLUGIN(PP2ZB_hadron);
}

