#include "Pythia8/Pythia.h"
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "Pythia8Plugins/Pythia8Rivet.h"

using namespace Pythia8;


int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        std::cout << "Wrong number of argumenst " << argc - 1 << " != 3 is provided!\n";

        std::cout << "You need to type <n_events>, <seed>, <yoda_name>\n";

        abort();
    }

    int n_events = std::stoi(argv[1]);

    int seed = std::stoi(argv[2]);

    std::string yoda_name = argv[3];
  
    //Generator
    Pythia pythia;

    // Set a seed to initialize a new sequence of pseudo-random numbers
    pythia.readString("Random:seed = " + std::to_string(seed));

    // Generating Z+j production
    pythia.readString("WeakBosonAndParton:qg2gmZq = on"); // Z/gamma+q production
    pythia.readString("WeakBosonAndParton:qqbar2gmZg = on"); // Z/gamma+g, production
    pythia.readString("WeakZ0:gmZmode = 2"); //only consider contributions from Z
    pythia.readString("HadronLevel:all = on"); 
    pythia.readString("PartonLevel:MPI = on"); 

    // Force leptonic decay of Z ---> mu+mu-
    pythia.readString("23:onMode = off");
    pythia.readString("23:onIfAny = 13");

    // Initialisation, pp @ 13 TeV
    pythia.readString("Beams:eCM = 13000");
    pythia.readString("PhaseSpace:pTHatMin = 400");
    pythia.init();

    // pipe to  Rivet program.
    Pythia8Rivet rivet(pythia, yoda_name + ".yoda");

    rivet.addAnalysis("PP2ZB_hadron"); 

    for (int i = 0; i < n_events; ++i) 
    {
        if (!pythia.next()) continue;
    
        rivet();
    }

    rivet.done();

    return 0;
}
