#include "Pythia8/Pythia.h"
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "Pythia8Plugins/Pythia8Rivet.h"

using namespace Pythia8;

int main() {
   
   //Generator
   Pythia pythia;

   // Generating Z+j production
   pythia.readString("WeakBosonAndParton:qg2gmZq = on"); // Z/gamma+q production
   pythia.readString("WeakBosonAndParton:qqbar2gmZg = on"); // Z/gamma+g, production
   pythia.readString("WeakZ0:gmZmode = 2"); //only consider contributions from Z
   pythia.readString("HadronLevel:all = on"); 
   pythia.readString("PartonLevel:MPI = on"); 
   // Force leptonic decay of Z ---> e+e-, mu+mu-
   pythia.readString("23:onMode = off");
   pythia.readString("23:onIfAny = 13");

   // Initialisation, pp @ 13 TeV
   pythia.readString("Beams:eCM = 13000");
   pythia.readString("PhaseSpace:pTHatMin = 300");
   pythia.init();

  // pipe to  Rivet program.
  Pythia8Rivet rivet(pythia, "pp2zb_hadron.yoda");
  //rivet.addAnalysis("ATLAS_2020_I1788444:LMODE=MU");
  rivet.addAnalysis("PP2ZB_hadron"); 
  int nEvent = 1e5;
  
  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) 
  {
    if (!pythia.next()) continue;
     	rivet();
  }  
  
  rivet.done();
  return 0;
}
