rivet-#include "Rivet/Analysis.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"

#include "Rivet/Tools/Cuts.hh"
#include "fastjet/contrib/SoftDrop.hh"
#include "fastjet/JadePlugin.hh"

#include <fstream>
#include <valarray>
#include <vector>


using namespace fastjet;


namespace Rivet 
{

class JF_Oleh
{
public:
    // SD parameters:
    // commmon value of beta = 0., 1., 2.
    // common value of z_cut = 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4
    //
    JF_Oleh() : m_sd(0.0, 0.05, std::numeric_limits<double>::infinity())
    {
        m_plugin = new fastjet::JadePlugin();

        fastjet::JetDefinition m_jet_def = fastjet::JetDefinition(m_plugin);

        m_jet_def.delete_plugin_when_unused();
       
        m_recluster = { m_jet_def };

        m_sd.set_reclustering(true, &m_recluster);
    }

protected:
    fastjet::contrib::SoftDrop m_sd;

    JetDefinition::Plugin *m_plugin;

    fastjet::Recluster m_recluster;

public:
    PseudoJet jf_groomer(const PseudoJet &jet)
    {
        PseudoJet sdj_jet = m_sd(jet);
        return sdj_jet;
    }
    
};



class PP2ZB_JF : public Analysis, JF_Oleh
{
public:
    PP2ZB_JF() : Analysis("PP2ZB_JF") {}
    
    // Jet Radius
    const double _jetR = 0.4;
    // Jet Rapidity 
    const double _jet_rap = 2.5;
    
    void init () 
    {
    // Initialise and register projections
    FinalState fs( Cuts::abseta < 4.5 );
    
    // for the muons
    double mu_pt = 27.0;
    double mz_min = (90-15);
    double mz_max = (90+15);
    double mu_eta_max = 2.5;
    
    // for histos
    int bins = 20;
    int bins_range_low = 300;
    int bins_range_high = 1000;

    ZFinder zfinder(fs,Cuts::pT > mu_pt*GeV  && Cuts::abseta < mu_eta_max, PID::MUON, mz_min*GeV, mz_max*GeV);
    declare(zfinder, "zfinder");

    // exclude leptons from jets
    // Particles for the jets
    VetoedFinalState jet_input(fs);
    jet_input.vetoNeutrinos();
    jet_input.addVetoOnThisFinalState(zfinder);
    declare(jet_input, "jet_input");
        
    // Book histograms:
    book( _h_ZpT , "ZpT", bins, bins_range_low, bins_range_high); 

    // Ungroomed jets    
    book( _h_bpT, "bpT", bins, bins_range_low, bins_range_high); 
    book( _h_bY, "bY", bins, -3, 3);

    // Softdrop jets
    book( _h_bpT_sd, "bpT_sd", bins, bins_range_low, bins_range_high);
    book( _h_bY_sd, "bY_sd", bins, -3, 3);

    // Softdrop+Jade jets
    book( _h_bpT_sdj, "bpT_sdj", bins, bins_range_low, bins_range_high); 
    book( _h_bY_sdj, "bY_sdj", bins, -3, 3);

    // outfiles to count the false negatives for naive counting, sd, and sd+j
    outFile1.open("naive_count.dat");
    outFile2.open("sd_count.dat");
    outFile3.open("sdj_count.dat");

   }
        
    void analyze(const Event &event) 
    {
    const double weight = event.weight();

    // Reconstruct Z boson
    const ZFinder &zfinder = apply<ZFinder>(event, "zfinder");   
    if ( zfinder.bosons().size() < 1 ) 	vetoEvent;
    const Particle &z = zfinder.bosons()[0];
    double mmtm = z.pt();	
    _h_ZpT->fill(mmtm, weight);


    // fastjet analysis
    JetDefinition jet_def(antikt_algorithm, _jetR);

    const VetoedFinalState &jet_input = apply<VetoedFinalState>(event, "jet_input");
    vector<PseudoJet> fjInputs;
    fjInputs.resize(0);

    int index = 0;
    std::valarray<double> fourvec(4);
    for ( const Particle &p: jet_input.particles() )
	{ 
	index = p.pid();

	fourvec[0] = p.px();
        fourvec[1] = p.py();
        fourvec[2] = p.pz();
        fourvec[3] = p.E() ;

	PseudoJet pseudoj(fourvec);
        pseudoj.set_user_index(index);
        fjInputs.push_back(pseudoj);
	}

    // Run Fastjet algorithm
    ClusterSequence clustSeq(fjInputs, jet_def);

	// Extract inclusive jets sorted by pT (min cut taken 400 GeV) and |rapidity| < 2.5
	vector<PseudoJet> sortedJets_noeta = sorted_by_pt(clustSeq.inclusive_jets(400));
	if ( sortedJets_noeta.size() < 1)	vetoEvent;

	vector<PseudoJet> sortedJets;
	for ( int i = 0; i < sortedJets_noeta.size(); ++i )
	{
		if ( (sortedJets_noeta[i].rapidity() < 2.5)&&(sortedJets_noeta[i].rapidity() > -2.5) ) 	sortedJets += sortedJets_noeta[i] ;
	}

	if (sortedJets.size() < 1)		vetoEvent;

	//Take the leading jet and do naive, sd, and sd+j
	PseudoJet leadingJet = sortedJets[0];

	//naive count
	fill_hist(leadingJet, outFile1, weight, _h_bpT, _h_bY);

	// sd the leading jet
	contrib::SoftDrop sd( 0.0, 0.05 );
	PseudoJet sdJet = sd(leadingJet);
	fill_hist(sdJet, outFile2, weight, _h_bpT_sd, _h_bY_sd);

	// sd+j the leading jet
	PseudoJet sdjJet = jf_groomer(leadingJet);
	fill_hist(sdjJet, outFile3, weight, _h_bpT_sdj, _h_bY_sdj);
    }
            
    void finalize() 
    {    
	    outFile1.close();
	    outFile2.close();
	    outFile3.close();
    }
            
private: 
    //Histograms
    Histo1DPtr _h_ZpT;   

    Histo1DPtr _h_bpT, _h_bY; //ungroomed
    Histo1DPtr _h_bpT_sd, _h_bY_sd; //groomed sd
    Histo1DPtr _h_bpT_sdj, _h_bY_sdj; //groomed sd+jade
 
    std::ofstream outFile1;
    std::ofstream outFile2;
    std::ofstream outFile3;
    
    // define function to fill hists and determine b-jets
    void fill_hist(const PseudoJet &leading, std::ofstream &outF, const double &w, Histo1DPtr _h_pt, Histo1DPtr _h_y)
    {   
            int Bcount = 0, Bquarks = 0, antiBquarks = 0;
	    //to keep track of misidentified
	    int false_negative = 0;

	    for ( int j = 0; j < leading.constituents().size(); ++j)
	    {
	        // Find net b from constituents	   
	        if (leading.constituents()[j].user_index() == 5)
	        {  
		    Bcount++;
		    Bquarks++;
	        }
	        
                if (leading.constituents()[j].user_index() == -5) 
	        {
		    Bcount--;
		    antiBquarks++;
  	        }	
   	    }
	
	    if ( Bcount == 0 ) { false_negative = 1; }
        
	    if ( Bcount == 1 || Bcount == -1)
	    {	
		_h_pt->fill(leading.perp(), w);
        	_h_y->fill(leading.rap(), w);
	    }  
        	//count how many times rivet misidentifies 
		outF << false_negative << endl;
	}
        

        //done
  
   };
    
    DECLARE_RIVET_PLUGIN(PP2ZB_JF);
}

