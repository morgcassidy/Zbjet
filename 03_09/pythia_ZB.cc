#include "Pythia8/Pythia.h"
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "Pythia8Plugins/Pythia8Rivet.h"

using namespace Pythia8;

int main() {
   
   //Generator
   Pythia pythia;

   // Generating Z+j production
   pythia.readString("WeakBosonAndParton:qg2gmZq = on"); // Z/gamma+q production
   pythia.readString("WeakBosonAndParton:qqbar2gmZg = on"); // Z/gamma+g, production
   pythia.readString("WeakZ0:gmZmode = 2"); //only consider contributions from Z
   pythia.readString("HadronLevel:all = on"); 
   pythia.readString("PartonLevel:MPI = on"); 
   // Force leptonic decay of Z ---> e+e-, mu+mu-
   pythia.readString("23:onMode = off");
   pythia.readString("23:onIfAny = 13");

   // Initialisation, pp @ 13 TeV
   pythia.readString("Beams:eCM = 13000");
   pythia.readString("PhaseSpace:pTHatMin = 200");
   pythia.init();

  // pipe to  Rivet program.
  Pythia8Rivet rivet(pythia, "pp2zb_hadron40.yoda");
  rivet.addAnalysis("PP2ZB_hadron"); 
  int nEvent = 1e4;
  int bquark_event = 0;
  int lightquark_event = 0;
  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    if (!pythia.next()) continue;
    
    //Find events with hard b-quarks
    bool has_hard_b = false;
    for (int i = 0; i < pythia.event.size(); ++i)
    {
	    if (pythia.event[i].statusAbs() == 23 && pythia.event[i].idAbs() == 5)
	    {
	      has_hard_b = true; 
	      break;
	    }
    }
    
    // Events with no hard b
    if( has_hard_b == false) 
    {
	    ++lightquark_event;
//	    rivet();
    }

    // if has hard b, push to rivet
    if( has_hard_b == true)
    {
	++bquark_event;
     	rivet();
	
    }
  
  }  
  cout << "number of light quark/ gluon events from pythia: " << lightquark_event << endl;
  cout << "number of bquark events from pythia: " << bquark_event << endl;
  rivet.done();
  return 0;
}
