// Written by Oleh Fedkevych
// oleh.fedkevych@ge.infn.it 
// The Angularity class is by Gregory Soyez / CMS Z+jet analysis
// To compile it do not forget to add  -lRecursiveTools flag 
#include "Rivet/Analysis.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"

#include "Rivet/Tools/Cuts.hh"

#include "fastjet/tools/Filter.hh"
#include "fastjet/contrib/Nsubjettiness.hh"
#include "fastjet/tools/Recluster.hh"
#include "fastjet/contrib/SoftDrop.hh"
#include "fastjet/contrib/IteratedSoftDrop.hh"

#include <cmath>
#include <fstream>
#include <vector>
#include <utility>
#include <iomanip>
#include <sstream>

using std::cout;
using std::vector;
using std::string;
using std::pair;
using std::setw;

using namespace fastjet;

/// \class SD_array
// A simple helper class to perform scan over different SoftDrop parameters
class SD_array 
{
public:
    SD_array () : sd_gr_b0_z005 (0.0, 0.05, 0.8),
                  sd_gr_b0_z01  (0.0, 0.10, 0.8),
                  sd_gr_b0_z015 (0.0, 0.15, 0.8),
                  sd_gr_b0_z02  (0.0, 0.20, 0.8),
                  sd_gr_b0_z025 (0.0, 0.25, 0.8),
                  sd_gr_b0_z03  (0.0, 0.30, 0.8), 
                  sd_gr_b0_z035 (0.0, 0.35, 0.8), 
                  sd_gr_b0_z04  (0.0, 0.40, 0.8), 
                      
                  sd_gr_b1_z005 (1.0, 0.05, 0.8),
                  sd_gr_b1_z01  (1.0, 0.10, 0.8),
                  sd_gr_b1_z015 (1.0, 0.15, 0.8),
                  sd_gr_b1_z02  (1.0, 0.20, 0.8),
                  sd_gr_b1_z025 (1.0, 0.25, 0.8),
                  sd_gr_b1_z03  (1.0, 0.30, 0.8), 
                  sd_gr_b1_z035 (1.0, 0.35, 0.8), 
                  sd_gr_b1_z04  (1.0, 0.40, 0.8), 
                      
                  sd_gr_b2_z005 (2.0, 0.05, 0.8),
                  sd_gr_b2_z01  (2.0, 0.10, 0.8),
                  sd_gr_b2_z015 (2.0, 0.15, 0.8),
                  sd_gr_b2_z02  (2.0, 0.20, 0.8),
                  sd_gr_b2_z025 (2.0, 0.25, 0.8),
                  sd_gr_b2_z03  (2.0, 0.30, 0.8), 
                  sd_gr_b2_z035 (2.0, 0.35, 0.8), 
                  sd_gr_b2_z04  (2.0, 0.40, 0.8) { }
protected:
    contrib::SoftDrop sd_gr_b0_z005;
    contrib::SoftDrop sd_gr_b0_z01;
    contrib::SoftDrop sd_gr_b0_z015;
    contrib::SoftDrop sd_gr_b0_z02;
    contrib::SoftDrop sd_gr_b0_z025;
    contrib::SoftDrop sd_gr_b0_z03;
    contrib::SoftDrop sd_gr_b0_z035;
    contrib::SoftDrop sd_gr_b0_z04;
    
protected:
    contrib::SoftDrop sd_gr_b1_z005;
    contrib::SoftDrop sd_gr_b1_z01;
    contrib::SoftDrop sd_gr_b1_z015;
    contrib::SoftDrop sd_gr_b1_z02;
    contrib::SoftDrop sd_gr_b1_z025;
    contrib::SoftDrop sd_gr_b1_z03;
    contrib::SoftDrop sd_gr_b1_z035;
    contrib::SoftDrop sd_gr_b1_z04;

protected:
    contrib::SoftDrop sd_gr_b2_z005;
    contrib::SoftDrop sd_gr_b2_z01;
    contrib::SoftDrop sd_gr_b2_z015;
    contrib::SoftDrop sd_gr_b2_z02;
    contrib::SoftDrop sd_gr_b2_z025;
    contrib::SoftDrop sd_gr_b2_z03;
    contrib::SoftDrop sd_gr_b2_z035;
    contrib::SoftDrop sd_gr_b2_z04;
        
public: 
    PseudoJet groomer(const double beta, const double z_cut, const PseudoJet &jet) 
    {
        PseudoJet sd_jet;
            
        if (beta == 0.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b0_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b0_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b0_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b0_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b0_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b0_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b0_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b0_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else if (beta == 1.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b1_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b1_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b1_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b1_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b1_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b1_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b1_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b1_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else if (beta == 2.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b2_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b2_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b2_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b2_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b2_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b2_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b2_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b2_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else
        {
            std::cout << "Error: bete = " << beta << " and is out of range!\n";
                    
            abort();
        }
            
        return sd_jet;
    }
};
//---------------------------------------------------------------------------------------------------------------------
namespace Rivet 
{
class PP2ZB : public Analysis, SD_array
{
public:
    PP2ZB() : Analysis("PP2ZB") {} 
    
    // Jet Radius
    const double _jetR = 0.4;
    // Jet Rapidity 
    const double _jet_rap = 2.5;
    
    // SD parameters:
    // Choose a value of beta = 0., 1., 2.
    const double _beta = 0.0;
    // Choose a value of z_cut = 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4
    const double _z_cut = 0.1;
           
    // Set the pT-bin 
    const double _pTJmin = 500.;
    const double _pTJmax = 800.;
    
    void init () 
    {
        // Initialise and register projections
       FinalState fs( Cuts::abseta < 5.0 );
    
        // for the muons
        double mu_pt = 27.0;
        double mz_min = (90-20);
        double mz_max = (90+20);
        double mu_eta_max = 2.4;
    
	//for electrons
	double el_pt = 27.0;
	double el_eta_max = 2.47;

        ZFinder zfinderM(fs,Cuts::pT > mu_pt*GeV  && Cuts::abseta < mu_eta_max, PID::MUON, mz_min*GeV, mz_max*GeV);
        declare(zfinderM, "zfinderM");

	ZFinder zfinderE(fs, Cuts::pT > el_pt*GeV  && Cuts::abseta < el_eta_max, PID::ELECTRON, mz_min*GeV, mz_max*GeV);
        declare(zfinderE, "zfinderE");

	// exclude leptons from jets
        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
	jet_input.addVetoOnThisFinalState(zfinderM);
	jet_input.addVetoOnThisFinalState(zfinderE);
	declare(jet_input, "jet_input");
        
	// FastJet projections
//	FastJets jets(fjInputs, FastJets::ANTIKT, _jetR);
// 	declare(jets, "jets");
        
        // Book histograms:
	book( _h_ZpT , "ZpT", 60, 0.0, 1000); 
	
	// Ungroomed
	book( _h_bpT, "bpT", 60, 0, 700); 
	book( _h_bY, "bY", 60, -8, 8);
        // Groomed
	book( _h_sdbpT, "sd_bpT", 60, 0, 700);
	book( _h_sdbY, "sd_bY", 60, -8, 8);
   }
        
    void analyze(const Event &event) 
    {
        // Reconstruct Z-boson
        const ZFinder &zfinderE = apply<ZFinder>(event, "zfinderE");
        const Particles &els = zfinderE.constituents();
        
	const ZFinder &zfinderM = apply<ZFinder>(event, "zfinderM");   
	const Particles &mus = zfinderM.constituents();

	if((els.size() + mus.size()) != 2 ) vetoEvent;
	if( !(els.size() == 2) || !(mus.size() == 2) ) vetoEvent;

 	double zpt = 0;
	if( els.size() == 2 )
	{
       		const Particle &z = zfinderE.bosons()[0];
        	zpt = z.pt();
		_h_ZpT->fill(zpt);
	}
        if( mus.size() == 2 )
	{	
		const Particle &z = zfinderM.bosons()[0]; 
		zpt = z.pt();
		_h_ZpT->fill(zpt);
	}

	// fastjet analysis parameters
	JetDefinition jet_def(antikt_algorithm, _jetR);
        // fastjet input
	//std::vector fastjet::PseudoJet fjInputs;

	const VetoedFinalState &jet_input = apply<VetoedFinalState>(event, "jet_input");
	PseudoJets fjInputs;
	int index = 0;
	std::valarray<double> fourvec(4);
	for ( const Particle &p: jet_input.particles() )
	{ 
		index = p.pid();
		fourvec[0] = p.px();
	        fourvec[1] = p.py();
	        fourvec[2] = p.pz();
	        fourvec[3] = p.E() ;
		fastjet::PseudoJet pseudoj(fourvec);
		pseudoj.set_user_index(index);
		fjInputs.push_back(pseudoj);
	}
         // Run Fastjet algorithm
        ClusterSequence clustSeq(fjInputs, jet_def); 

	 // Extract inclusive jets sorted by pT (min cut taken 25 GeV)
    	PseudoJets sortedJets = sorted_by_pt(clustSeq.inclusive_jets(25));
	
	//get jet constituents
        for ( size_t i = 0; i < sortedJets.size(); ++i)
       {	
	//find jet constituents
	PseudoJets constituents = sortedJets[i].constituents();
	int Bcount = 0;
	int Bquarks = 0;
	int antiBquarks = 0;
	for ( int j = 0; j < 1; ++j)
	{
   	   // Find net b from constituents	   
	   if (constituents[j].user_index() == 5)
	   {  
		   Bcount++;
		   Bquarks++;
	   }
	   if (constituents[j].user_index() == -5) 
	   {
		   Bcount--;
		   antiBquarks++;
  	   }	
   	}
	cout << "Bcount: [" << Bcount << "]	B: [" << Bquarks << "]	antiBs: [" << antiBquarks << "]" << endl;
	if ( Bcount == 1 || Bcount == -1)
	   {	
		_h_bpT->fill(sortedJets[i].perp()/GeV);
	        _h_bY->fill(sortedJets[i].rap());

		//PseudoJet sd_jet = groomer(_beta, _z_cut, sortedJets[i]);
		contrib::SoftDrop sd(_beta, _z_cut);
		PseudoJet sd_jet = sd(sortedJets[i]);
		_h_sdbpT->fill(sd_jet.perp()/GeV);
		_h_sdbY->fill(sd_jet.rap());
	   }  
	
       }
    }
            
    void finalize() {}
            
private: 
    Histo1DPtr _h_ZpT;   
   // SD histograms

    // Ungroomed
    Histo1DPtr _h_bpT, _h_bY;
    
    // Groomed 
    Histo1DPtr _h_sdbpT, _h_sdbY;
   
   };
    
    DECLARE_RIVET_PLUGIN(PP2ZB);
}

