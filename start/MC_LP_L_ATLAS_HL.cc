// Written by Oleh Fedkevych
// oleh.fedkevych@ge.infn.it 
// The Angularity class is by Gregory Soyez / CMS Z+jet analysis
// To compile it do not forget to add  -lRecursiveTools flag 
#include "Rivet/Analysis.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"

#include "Rivet/Tools/Cuts.hh"

#include "fastjet/contrib/LundGenerator.hh"

#include "fastjet/tools/Recluster.hh"
#include "fastjet/contrib/SoftDrop.hh"
#include "fastjet/contrib/IteratedSoftDrop.hh"

#include <cmath>
#include <fstream>
#include <vector>
#include <utility>
#include <iomanip>
#include <sstream>

using std::cout;
using std::vector;
using std::string;
using std::pair;
using std::setw;

using namespace fastjet;

/// \class Angularity
/// definition of angularity
///
class Angularity : public FunctionOfPseudoJet<double>{
public:
  /// ctor
  Angularity(double alpha, double jet_radius, double kappa=1.0, Selector constitCut=SelectorPtMin(0.)) : _alpha(alpha), _radius(jet_radius), _kappa(kappa), _constitCut(constitCut) {}

  /// description
  std::string description() const{
    std::ostringstream oss;
    oss << "Angularity with alpha=" << _alpha;
    return oss.str();
  }

  /// computation of the angularity itself
  double result(const PseudoJet &jet) const{
    // get the jet constituents
    vector<PseudoJet> constits = jet.constituents();

    // get the reference axis
    PseudoJet reference_axis = _get_reference_axis(jet);

    // do the actual coputation
    double numerator = 0.0, denominator = 0.0;
    unsigned int num = 0;
    for (const auto &c : constits){
      if (!_constitCut.pass(c)) continue;
      double pt = c.pt();
      // Note: better compute (dist^2)^(alpha/2) to avoid an extra square root
      numerator   += pow(pt, _kappa) * pow(c.squared_distance(reference_axis), 0.5*_alpha);
      denominator += pt;
      num += 1;
    }
    if (denominator == 0) return -1;
    // the formula is only correct for the the typical angularities which satisfy either kappa==1 or alpha==0.
    else return numerator/(pow(denominator, _kappa)*pow(_radius, _alpha));
  }

protected:
  PseudoJet _get_reference_axis(const PseudoJet &jet) const{
    if (_alpha>1) return jet;

    Recluster recluster(JetDefinition(antikt_algorithm, JetDefinition::max_allowable_R, WTA_pt_scheme));
    return recluster(jet);
  }

  double _alpha, _radius, _kappa;
  Selector _constitCut;
};

/// \class SD_array
// A simple helper class to perform scan over different SoftDrop parameters
class SD_array 
{
public:
    SD_array () : sd_gr_b0_z005 (0.0, 0.05, 0.8),
                  sd_gr_b0_z01  (0.0, 0.10, 0.8),
                  sd_gr_b0_z015 (0.0, 0.15, 0.8),
                  sd_gr_b0_z02  (0.0, 0.20, 0.8),
                  sd_gr_b0_z025 (0.0, 0.25, 0.8),
                  sd_gr_b0_z03  (0.0, 0.30, 0.8), 
                  sd_gr_b0_z035 (0.0, 0.35, 0.8), 
                  sd_gr_b0_z04  (0.0, 0.40, 0.8), 
                      
                  sd_gr_b1_z005 (1.0, 0.05, 0.8),
                  sd_gr_b1_z01  (1.0, 0.10, 0.8),
                  sd_gr_b1_z015 (1.0, 0.15, 0.8),
                  sd_gr_b1_z02  (1.0, 0.20, 0.8),
                  sd_gr_b1_z025 (1.0, 0.25, 0.8),
                  sd_gr_b1_z03  (1.0, 0.30, 0.8), 
                  sd_gr_b1_z035 (1.0, 0.35, 0.8), 
                  sd_gr_b1_z04  (1.0, 0.40, 0.8), 
                      
                  sd_gr_b2_z005 (2.0, 0.05, 0.8),
                  sd_gr_b2_z01  (2.0, 0.10, 0.8),
                  sd_gr_b2_z015 (2.0, 0.15, 0.8),
                  sd_gr_b2_z02  (2.0, 0.20, 0.8),
                  sd_gr_b2_z025 (2.0, 0.25, 0.8),
                  sd_gr_b2_z03  (2.0, 0.30, 0.8), 
                  sd_gr_b2_z035 (2.0, 0.35, 0.8), 
                  sd_gr_b2_z04  (2.0, 0.40, 0.8) { }
protected:
    contrib::SoftDrop sd_gr_b0_z005;
    contrib::SoftDrop sd_gr_b0_z01;
    contrib::SoftDrop sd_gr_b0_z015;
    contrib::SoftDrop sd_gr_b0_z02;
    contrib::SoftDrop sd_gr_b0_z025;
    contrib::SoftDrop sd_gr_b0_z03;
    contrib::SoftDrop sd_gr_b0_z035;
    contrib::SoftDrop sd_gr_b0_z04;
    
protected:
    contrib::SoftDrop sd_gr_b1_z005;
    contrib::SoftDrop sd_gr_b1_z01;
    contrib::SoftDrop sd_gr_b1_z015;
    contrib::SoftDrop sd_gr_b1_z02;
    contrib::SoftDrop sd_gr_b1_z025;
    contrib::SoftDrop sd_gr_b1_z03;
    contrib::SoftDrop sd_gr_b1_z035;
    contrib::SoftDrop sd_gr_b1_z04;

protected:
    contrib::SoftDrop sd_gr_b2_z005;
    contrib::SoftDrop sd_gr_b2_z01;
    contrib::SoftDrop sd_gr_b2_z015;
    contrib::SoftDrop sd_gr_b2_z02;
    contrib::SoftDrop sd_gr_b2_z025;
    contrib::SoftDrop sd_gr_b2_z03;
    contrib::SoftDrop sd_gr_b2_z035;
    contrib::SoftDrop sd_gr_b2_z04;
        
public: 
    PseudoJet groomer(const double beta, const double z_cut, const PseudoJet &jet) 
    {
        PseudoJet sd_jet;
            
        if (beta == 0.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b0_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b0_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b0_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b0_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b0_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b0_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b0_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b0_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else if (beta == 1.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b1_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b1_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b1_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b1_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b1_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b1_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b1_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b1_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else if (beta == 2.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b2_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b2_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b2_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b2_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b2_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b2_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b2_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b2_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else
        {
            std::cout << "Error: bete = " << beta << " and is out of range!\n";
                    
            abort();
        }
            
        return sd_jet;
    }
};
//---------------------------------------------------------------------------------------------------------------------
namespace Rivet 
{
class MC_LP_L_ATLAS_HL : public Analysis, SD_array
{
public:
    MC_LP_L_ATLAS_HL() : Analysis("MC_LP_L_ATLAS_HL") {} 
    
    // Jet Radius
    const double _jetR = 0.4;

    // Jet Rapidity 
    const double _jet_rap = 2.5;
    
    // SD parameters:
    // Choose a value of beta = 0., 1., 2.
    const double _beta = 0.0;
            
    // Choose a value of z_cut = 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4
    const double _z_cut = 0.1;
            
    // Set bins (for log10(lambda) distributions)
    const int _ledge = -7;
            
    const int _redge = 0;
            
    const int _nbins = 100;
    
    // Set the pT-bin 
    const double _pTJmin = 500.;
    const double _pTJmax = 800.;
    
    void init () 
    {
        // Initialise and register projections
        FinalState fs(-5, 5, 0.0*GeV);
    
        // for the muons
        double mu_pt = 26.;
        double mz_min = (90-20);
        double mz_max = (90+20);
        double eta_max = 2.4;
    
        ZFinder zfinder(fs,
                    Cuts::pT > mu_pt*GeV  && Cuts::abseta < eta_max,
                    PID::MUON,
                    mz_min*GeV, mz_max*GeV,
                    0.1, ZFinder::NOCLUSTER, ZFinder::NOTRACK);
        addProjection(zfinder, "ZFinder");

        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
        jet_input.addVetoOnThisFinalState(getProjection<ZFinder>("ZFinder"));
        addProjection(jet_input, "JET_INPUT");
        
        // FastJet projections
        declare("AKTJETS", FastJets(jet_input, FastJets::ANTIKT, _jetR));
        
        // Book histograms:
        _h_lund_plane    = bookHisto2D("lund_plane",    50, 0, 7, 50, 0, 7);
        _h_lund_plane_sd = bookHisto2D("lund_plane_sd", 50, 0, 7, 50, 0, 7);
            
        // Ungroomed
        _h_05 = bookHisto1D( "LHA",    linspace(_nbins, _ledge, _redge) );
        _h_10 = bookHisto1D( "Width",  linspace(_nbins, _ledge, _redge) );
        _h_20 = bookHisto1D( "Thrust", linspace(_nbins, _ledge, _redge) );

        // Groomed
        _h_05_sd = bookHisto1D( "LHA_SD",    linspace(_nbins, _ledge, _redge) );
        _h_10_sd = bookHisto1D( "Width_SD",  linspace(_nbins, _ledge, _redge) );
        _h_20_sd = bookHisto1D( "Thrust_SD", linspace(_nbins, _ledge, _redge) );
            
        // Ungroomed lund plane
        _outFile.open("zjet_lp_ljet.dat");
        
        _outFile << "# Ungroomed: ln(1 / Delta_ij), ln(kt / GeV)\n";

        _outFile << std::setprecision(6) << std::scientific;
        
        _jet_count = 0;
        
        // Groomed lund plane
        _outFileSD.open("zjet_lp_ljet_sd.dat");
        
        _outFileSD << "# Groomed: ln(1 / Delta_ij), ln(kt / GeV)\n";

        _outFileSD << std::setprecision(6) << std::scientific;
        
        _jet_count_sd = 0;
    }
        
    void analyze(const Event &event) 
    {
        const double weight = event.weight();

        // Reconstruct Z-boson
        const ZFinder &zfinder = applyProjection<ZFinder>(event, "ZFinder");
            
        if (zfinder.bosons().size() < 1) vetoEvent;

        const Particle &z = zfinder.bosons()[0];
        
        double zpt = z.pt();

        // Now do selection criteria
        // Do b-tagging tagging
        const Jets jets = apply<FastJets>(event, "AKTJETS").jetsByPt(Cuts::pT > 15*GeV);
      
        Jets ltag_jets =  filter_select(jets, !hasBTag(Cuts::pT > 5*GeV) );
 
        if (ltag_jets.size() < 1) vetoEvent;
        
        // Now apply fiducial cuts
        bool passZpJ = false;
        
        PseudoJet jet1 = ltag_jets[0];
        
        double jet1pt = jet1.pt();
        
        double asym = fabs((jet1pt - zpt) / (jet1pt + zpt));
 
        double dphi = Rivet::deltaPhi(jet1.phi(), z.phi());
        
        passZpJ = ( (fabs(jet1.rapidity()) < _jet_rap) && (zpt > 30) && (asym < 0.3) && (dphi > 2.0) );

        if (!passZpJ) vetoEvent;

        if (jet1pt < _pTJmin) vetoEvent;
        
        if (jet1pt > _pTJmax) vetoEvent;
        
        // If comes here then accept event
        
        // Run SD on a leading jet 
        PseudoJet sd_jet = groomer(_beta, _z_cut, jet1);
 
        // Lund plane analysis
        fill_lund_plane (jet1, _h_05, _h_10, _h_20, _h_lund_plane,    weight, _outFile,   _jet_count);

        fill_lund_plane (sd_jet, _h_05_sd, _h_10_sd, _h_20_sd, _h_lund_plane_sd, weight, _outFileSD, _jet_count_sd);
    }
            
    void finalize() 
    {
        _outFile.close();
        
        _outFileSD.close();
    }
            
private:    
    contrib::LundGenerator _lund;
        
    // histograms
    Histo2DPtr _h_lund_plane, _h_lund_plane_sd;
        
    // SD histograms
    // Ungroomed
    Histo1DPtr _h_05, _h_10, _h_20;
    
    // Groomed 
    Histo1DPtr _h_05_sd, _h_10_sd, _h_20_sd;
    
    // Files to output pixels
    std::ofstream _outFile, _outFileSD;
    
    int _jet_count, _jet_count_sd;
    
    // Simpler helper function to get angularity
    double _get_ang (double alpha, const PseudoJet &jet) 
    {
        double res = 0.;
            
        Angularity angularity(alpha, _jetR);
                
        res = angularity(jet);
            
        return res;
    }

    // Fill all histograms at once and produce the training data
    void fill_lund_plane (const PseudoJet &jet, 
                          Histo1DPtr h1, Histo1DPtr h2, Histo1DPtr h3, 
                          Histo2DPtr h_lund, double wgt, std::ofstream &outF, int &njet)
    {
        double v05 = _get_ang(0.5, jet);
        double v10 = _get_ang(1.0, jet);
        double v20 = _get_ang(2.0, jet);
        
        // we do log10 plots 
        // so we out the angularities with zero values (a single particle jet) in the first bin
        if      (v05 > 0. ) h1->fill(std::log10(v05), wgt);
        else if (v05 == 0.) h1->fill(_ledge + 1e-3,   wgt);
        
        if      (v10 > 0. ) h2->fill(std::log10(v10), wgt);
        else if (v10 == 0.) h2->fill(_ledge + 1e-3,   wgt);
        
        if      (v20 > 0. ) h3->fill(std::log10(v20), wgt);
        else if (v20 == 0.) h3->fill(_ledge + 1e-3,   wgt);
        
        // now get the Lund Plane points
        vector<contrib::LundDeclustering> declusts = _lund(jet);
        
        if (declusts.size() > 0)
        {
            njet++;
            
            outF << "JETN = " << njet  << " size = " << declusts.size() 
                 << " weight = " << wgt 
                 << " L0 = "  << v05
                 << " L10 = " << v10
                 << " L20 = " << v20 << "\n";

        }
            
        // Fill Lund Plane histogram
        for (long unsigned int i = 0; i < declusts.size(); ++i) 
        {
            pair<double,double> coords = declusts[i].lund_coordinates();
                
            outF << setw(15) << coords.first << setw(15) << coords.second << "\n";
            
            h_lund->fill(coords.first, coords.second, wgt);
        }
    }

};
    
    DECLARE_RIVET_PLUGIN(MC_LP_L_ATLAS_HL);
}

