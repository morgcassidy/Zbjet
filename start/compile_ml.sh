#!/bin/bash

# Is you use FastJet::contrib (e.g. SoftDrop) do not forget to inlucde following compilation flags
# -lRecursiveTools -lfastjetcontribfragile -lfastjettools

LOC=/home/ofedkevych/

RIV=$LOC/RIVET2

YOD=$LOC/yoda-1.7.7

HEP=$LOC/hepmc2

FJT=$LOC/fastjet

echo 'Compiling CMS plugins...'

echo 'lund plane...'

echo 'b-jet...'

g++ -o "RivetMC_LP_B_CMS_PL.so" -shared -fPIC  -I$RIV/include -I$YOD/include -I$HEP/include -I$FJT/include   -pedantic -Wall -Wno-deprecated-declarations -Wno-long-long -Wno-format -Werror=uninitialized -Wno-reorder -Werror=delete-non-virtual-dtor -fopenmp -O2   -Wl,--no-as-needed  -L$RIV/lib -L$HEP/lib -L$YOD/lib -Wl,-rpath,$FJT/lib -L$FJT/lib -lfastjettools -lfastjet -lm -lfastjetplugins -lsiscone_spherical -lsiscone  -lRecursiveTools -lfastjetcontribfragile -lRivet  MC_LP_B_CMS_PL.cc

g++ -o "RivetMC_LP_B_CMS_HL.so" -shared -fPIC  -I$RIV/include -I$YOD/include -I$HEP/include -I$FJT/include   -pedantic -Wall -Wno-deprecated-declarations -Wno-long-long -Wno-format -Werror=uninitialized -Wno-reorder -Werror=delete-non-virtual-dtor -fopenmp -O2   -Wl,--no-as-needed  -L$RIV/lib -L$HEP/lib -L$YOD/lib -Wl,-rpath,$FJT/lib -L$FJT/lib -lfastjettools -lfastjet -lm -lfastjetplugins -lsiscone_spherical -lsiscone  -lRecursiveTools -lfastjetcontribfragile -lRivet  MC_LP_B_CMS_HL.cc

echo '...done!'

