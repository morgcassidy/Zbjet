#include "Pythia8/Pythia.h"
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "Pythia8Plugins/Pythia8Rivet.h"

using namespace Pythia8;

int main() {
   
   //Generator
   Pythia pythia;

   // Generating Z+j production
   pythia.readString("WeakBosonAndParton:qg2gmZq = on"); // Z/gamma+q production
   pythia.readString("WeakZ0:gmZmode = 2"); //only consider contributions from Z
   pythia.readString("HadronLevel:all = off"); // stop process after hard and shower
   pythia.readString("PartonLevel:MPI = off"); // turn off mulit-parton interactions for simplification
 
   // Force leptonic decay of Z ---> lept(e+/mu+) lept(e-/mu-)
   pythia.readString("23:onMode = off");
   pythia.readString("23:onIfAny = 11 13");
   //pythia.readString("23:mMin = 60");

   // Initialisation, pp @ 13 TeV
   pythia.readString("Beams:eCM = 13000");
   pythia.init();


  // pipe to  Rivet program.
  Pythia8Rivet rivet(pythia, "pp2zb.yoda");
  rivet.addAnalysis("ATLAS_2020_I1788444");

  // Book histogram
  Hist pT("jet pT", 100, 0, 300);

   // Fastjet analysis - select algorithm and parameters
   double Rparam = 0.4;
   fastjet::Strategy               strategy = fastjet::Best;
   fastjet::RecombinationScheme    recombScheme = fastjet::E_scheme;
   fastjet::JetDefinition         *jetDef = NULL;
   jetDef = new fastjet::JetDefinition(fastjet::antikt_algorithm, Rparam,
                                      recombScheme, strategy);
   // Fastjet input
   std::vector <fastjet::PseudoJet> fjInputs;
 
  int nEvent = 1e4;
  bool firstEvent = true;
  // Begin event loop. Generate event. Skip if error. List first one.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    if (!pythia.next()) continue;

    // push to Rivet
    rivet();

    //reset fasstjet input
    fjInputs.resize(0); 

    //Find events with a hard b 
    bool has_hard_b = false;
    for (int i = 0; i < pythia.event.size(); ++i)
    {
	    if (pythia.event[i].statusAbs() == 23 && pythia.event[i].idAbs() == 5 && pythia.event[i].mother1() == 3 && pythia.event[i].mother2() == 4)
	    {
	      has_hard_b = true; 
	      break;
	    }
    }
    
    // Events with no hard b
    if( has_hard_b ) cout << "Event[" << iEvent << "] has no hard b quark." << endl;

    // pass particles to fastjet for events containing a hard b
    if( has_hard_b )
    {   
	std::valarray<double> fourvec(4);
        int index = 0;	
        for(int i = 0; i < pythia.event.size(); ++i) //loop over all particles in current event
	{
           // No neutrinos
           if (pythia.event[i].idAbs() == 12 || pythia.event[i].idAbs() == 14 || pythia.event[i].idAbs() == 16)     continue;
	  
	   // No Lepts
           if (pythia.event[i].idAbs() == 11 || pythia.event[i].idAbs() == 13 || pythia.event[i].idAbs() == 15)     continue;
	
	 // Final State only
	 if ( !pythia.event[i].isFinal() )	continue;

	   //create particle and set index
           fourvec[0] = pythia.event[i].px();
	   fourvec[1] = pythia.event[i].py();
	   fourvec[2] = pythia.event[i].pz();
	   fourvec[3] = pythia.event[i].e();
	   index = pythia.event[i].id();
	   fastjet::PseudoJet particle(fourvec);
	   particle.set_user_index(index);
       
	   //store input into fastjet:
	   fjInputs.push_back(particle);	
	}
    }

     //do nothing with empty input
     if (fjInputs.size() == 0) continue;
	 
     // Run Fastjet algorithm
     vector <fastjet::PseudoJet> inclusiveJets, sortedJets;
     fastjet::ClusterSequence clustSeq(fjInputs, *jetDef);

     // For the first event, print the FastJet details
     if (firstEvent) {
      cout << "Ran " << jetDef->description() << endl;
      cout << "Strategy adopted by FastJet was "
           << clustSeq.strategy_string() << endl << endl;
      firstEvent = false;
    }

    // Extract inclusive jets sorted by pT (min cut taken 25 GeV)
    inclusiveJets = clustSeq.inclusive_jets(25);
    sortedJets = sorted_by_pt(inclusiveJets);
 
    //get jet constituents
    for ( int i = 0; i < sortedJets.size(); ++i)
    {	
	pT.fill(sortedJets[i].perp());    

	//find jet constituents
	vector <fastjet::PseudoJet> constituents = sortedJets[i].constituents();
	int Bcount = 0;
	int Bquarks = 0;
	int antiBquarks = 0;
	for ( int j = 0; j < 1; ++j)
	{
//	   cout << "Jet [" << i << "] constituent [" << j << "] has ID: " << constituents[j].user_index() << endl; 
      	
   	   // Find net b from constituents	   
	   if (constituents[j].user_index() == 5)
	   {  
		   Bcount++;
		   Bquarks++;
	   }
	   if (constituents[j].user_index() == -5) 
	   {
		   Bcount--;
		   antiBquarks++;
  	   }		    
    //	cout << "Net B value: [" << Bcount << "]" << endl;
    	}
	cout << "Event: [" << iEvent  << "] " << endl;
	cout << "Bquark total: [" << Bquarks << "]	antiB total: [" << antiBquarks << "]	net B value: [" << Bcount << "]" << endl;
	cout << endl;
    }
	// End of event loop. Statistics. Histogram. Delete jet def.
}
  pythia.stat();
  cout << pT;
  delete jetDef;
  rivet.done();


  return 0;
}
