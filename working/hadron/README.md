# Some notes
The pythia_ZB.cc is the script I'm using to run the rivet routine PP2ZB_hadron.cc. I've included some histograms for: a.) when I take a jet pT cut of 500 GeV, per Oleh's recommendation, but I think, as appears to be the case in the histograms, this discards basically all events containing the softer of the two Zb diagram contributions, ie, qqbar -> Zg, g -> bbar. So, I also just when ahead and included b.) histograms for when I take a jet pT cut of 20 GeV, following the cut given in Frederico's Rivet analysis, just to also see the distributions of bjet pT when there are 2 bjets from the gluon splitting. 

# The selection cuts used in the rivet analysis
final state particles:
|eta| < 4.5

muon from Z decay:
pT > 27 GeV
|eta| < 2.5

jets:
|eta| < 2.5
pT > 500 GeV (for the high pT run)
pT > 20 GeV (for the low pT run to see the 2 bjet pT and eta distributions)

number of events generated in pythia: 4e4
