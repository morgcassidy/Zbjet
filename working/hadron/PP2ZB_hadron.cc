
#include "Rivet/Analysis.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"

#include "Rivet/Tools/Cuts.hh"

#include "fastjet/contrib/SoftDrop.hh"


#include <fstream>
#include <valarray>



using namespace fastjet;
//---------------------------------------------------------------------------------------------------------------------
namespace Rivet 
{
class PP2ZB_hadron : public Analysis
{
public:
    PP2ZB_hadron() : Analysis("PP2ZB_hadron") {} 
    
    // Jet Radius
    const double _jetR = 0.4;
    // Jet Rapidity 
    const double _jet_rap = 2.5;
    
    // SD parameters:
    // Choose a value of beta = 0., 1., 2.
    const double _beta = 0.0;
    // Choose a value of z_cut = 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4
    const double _z_cut = 0.1;
     

    void init () 
    {
        // Initialise and register projections
       FinalState fs( Cuts::abseta < 4.5 );
    
        // for the muons
        double mu_pt = 27.0;
        double mz_min = (90-15);
        double mz_max = (90+15);
        double mu_eta_max = 2.5;
    
        ZFinder zfinder(fs,Cuts::pT > mu_pt*GeV  && Cuts::abseta < mu_eta_max, PID::MUON, mz_min*GeV, mz_max*GeV);
        declare(zfinder, "zfinder");

	// exclude leptons from jets
        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
	jet_input.addVetoOnThisFinalState(zfinder);
	declare(jet_input, "jet_input");
        
	//fastjet projection
	 FastJets jets(jet_input, FastJets::ANTIKT, _jetR);
	 declare(jets, "jets");

        // softdrop jets
	
	 
	// Book histograms:
	// Ungroomed
	// 1 bjet
	book( _h_ZpT , "ZpT", 60, 0.0, 1500); 
	book( _h_bpT, "bpT", 60, 0, 1500); 
	book( _h_bY, "bY", 60, -4, 4);
	
	//2 bjets
        book( _h_bY_2b , "bY_2b" , 60, -4, 4);
	book( _h_bpT_2b, "bpT_2b", 60 , 0, 1500);
        
	
	// Groomed
	//book( _h_sdbpT, "sd_bpT", 60, 0, 2000);
	//book( _h_sdbY, "sd_bY", 60, -4, 4);
        
	//set output file name
	outFile.open("test_output.dat");
   }
        
    void analyze(const Event &event) 
    {
        // Oleh: we should also not forget to work with event weights
        const double weight = event.weight();

        // Reconstruct Z-boson
	const ZFinder &zfinder = apply<ZFinder>(event, "zfinder");   

	if (zfinder.bosons().size() < 1) vetoEvent;	
 	
	const Particle &z = zfinder.bosons()[0]; 
	double zpt = z.pt();
   	
      // for jets 	
      const Jets jets = apply<FastJets>(event, "jets").jetsByPt(Cuts::pT>20*GeV && Cuts::absrap < 2.5);
      Jets btag_jets = filter_select(jets, hasBTag( Cuts::pT > 5*GeV ));

      if ( btag_jets.size() < 1 )	vetoEvent;
      outFile << "Number of tagged bjets: " << btag_jets.size() << endl;
	
      double by = 0;
      double bpt = 0;
      // for 1 bjet
      if (btag_jets.size() == 1 )
      {
      by = btag_jets[0].rapidity();
      bpt = btag_jets[0].perp()/GeV;
      _h_bY->fill(by, weight);
      _h_bpT->fill(bpt, weight);
      _h_ZpT->fill(zpt, weight);
      }

      // for 2 bjet      
      if( btag_jets.size() > 1 )
      {
      by =( btag_jets[0].mom() + btag_jets[1].mom() ).rapidity();
      bpt = (  btag_jets[0].mom() + btag_jets[1].mom() ).perp()/GeV;
      _h_bY_2b->fill(by, weight);
      _h_bpT_2b->fill(bpt, weight);
      _h_ZpT->fill(zpt, weight);
      }
    }
            
    void finalize() { outFile.close(); }
            
private: 
    Histo1DPtr _h_ZpT;   
   
    // Ungroomed
    Histo1DPtr _h_bpT, _h_bY, _h_bpT_2b, _h_bY_2b;
    
    // Groomed 
    Histo1DPtr _h_sdbpT, _h_sdbY;
 
    // Oleh: define a stream to work with files
    std::ofstream outFile;

   };
    
    DECLARE_RIVET_PLUGIN(PP2ZB_hadron);
}

