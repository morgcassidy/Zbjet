#include "Rivet/Analysis.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"

#include "Rivet/Tools/Cuts.hh"
#include "fastjet/contrib/SoftDrop.hh"

#include <fstream>
#include <valarray>
#include <vector>


using namespace fastjet;


namespace Rivet 
{
class PP2ZB : public Analysis
{
public:
    PP2ZB() : Analysis("PP2ZB") {} 
    
    // Jet Radius
    const double _jetR = 0.4;
    // Jet Rapidity 
    const double _jet_rap = 2.5;
    
    // SD parameters:
    // Choose a value of beta = 0., 1., 2.
    const double _beta = 0.0;
    // Choose a value of z_cut = 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4
    const double _z_cut = 0.1;
    
    void init () 
    {
        // Initialise and register projections
       FinalState fs( Cuts::abseta < 4.5 );
    
        // for the muons
        double mu_pt = 27.0;
        double mz_min = (90-15);
        double mz_max = (90+15);
        double mu_eta_max = 2.5;
    
        ZFinder zfinder(fs,Cuts::pT > mu_pt*GeV  && Cuts::abseta < mu_eta_max, PID::MUON, mz_min*GeV, mz_max*GeV);
        declare(zfinder, "zfinder");

	// exclude leptons from jets
        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
	jet_input.addVetoOnThisFinalState(zfinder);
	declare(jet_input, "jet_input");
        
        // Book histograms:
	book( _h_ZpT , "ZpT", 60, 0.0, 1000); 
	
	// Ungroomed
	book( _h_bpT, "bpT", 60, 0, 700); 
	book( _h_bY, "bY", 60, -8, 8);

        // Groomed
	book( _h_sdbpT, "sd_bpT", 60, 0, 700);
	book( _h_sdbY, "sd_bY", 60, -8, 8);

        outFile.open("test_output.dat");
   }
        
    void analyze(const Event &event) 
    {
        const double weight = event.weight();

        // Reconstruct Z-boson
	const ZFinder &zfinder = apply<ZFinder>(event, "zfinder");   
	if ( zfinder.bosons().size() < 1 ) 	vetoEvent;
        const Particle &z = zfinder.bosons()[0];
	double mmtm = z.pt();
	
	// fastjet analysis
	JetDefinition jet_def(antikt_algorithm, _jetR);

	const VetoedFinalState &jet_input = apply<VetoedFinalState>(event, "jet_input");
        vector<PseudoJet> fjInputs;
        fjInputs.resize(0);

	int index = 0;
        std::valarray<double> fourvec(4);
        for ( const Particle &p: jet_input.particles() )
	{ 
		index = p.pid();

		fourvec[0] = p.px();
	        fourvec[1] = p.py();
	        fourvec[2] = p.pz();
	        fourvec[3] = p.E() ;

		PseudoJet pseudoj(fourvec);
                pseudoj.set_user_index(index);
                fjInputs.push_back(pseudoj);
	}

        // Run Fastjet algorithm
        ClusterSequence clustSeq(fjInputs, jet_def); 

	// Extract inclusive jets sorted by pT (min cut taken 500 GeV) and |rapidity| < 2.5
	vector<PseudoJet> sortedJets_noeta = sorted_by_pt(clustSeq.inclusive_jets(500));
	if ( sortedJets_noeta.size() < 1)	vetoEvent;

	vector<PseudoJet> sortedJets;
	for ( int i = 0; i < sortedJets_noeta.size(); ++i )
	{
		if ( (sortedJets_noeta[i].rapidity() < 2.5)&&(sortedJets_noeta[i].rapidity() > -2.5) ) 	sortedJets += sortedJets_noeta[i] ;
	}
	
	// take the leading jet
	PseudoJet leadingJet = sortedJets[0];
	
	//fill hists and find bjets
        fill_hist(leadingJet, outFile, weight);
   	_h_ZpT->fill(mmtm/GeV, weight);
    
    }
            
    void finalize() { outFile.close(); }
            
private: 
    //Histograms
    Histo1DPtr _h_ZpT;   
    Histo1DPtr _h_bpT, _h_bY; //ungroomed
    Histo1DPtr _h_sdbpT, _h_sdbY; //sd jets
 
    std::ofstream outFile;

    // define function to fill hists and determine b-jets
    void fill_hist(const PseudoJet &leading, std::ofstream &outF, const double &w)
    {   
	   // vector<PseudoJet> constituents = leading.constituents();
	
            int Bcount = 0, Bquarks = 0, antiBquarks = 0;
	    //to keep track of misidentified
	    int false_negative = 0, false_positive = 0;

	    for ( int j = 0; j < leading.constituents().size(); ++j)
	    {
	        // Find net b from constituents	   
	        if (leading.constituents()[j].user_index() == 5)
	        {  
		    Bcount++;
		    Bquarks++;
	        }
	        
                if (leading.constituents()[j].user_index() == -5) 
	        {
		    Bcount--;
		    antiBquarks++;
  	        }	
   	    }
	
	    if ( Bcount == 0 ) { false_negative = 1; }
        
	    if ( Bcount == 1 || Bcount == -1)
	    {	
		_h_bpT->fill(leading.perp()/GeV, w);
	        _h_bY->fill(leading.rap(), w);
  		// apply softdrop on b-jets
		contrib::SoftDrop sd(_beta, _z_cut);
		PseudoJet sd_jet = sd(leading);
		// fill sd hists
		_h_sdbpT->fill(sd_jet.perp()/GeV, w);
		_h_sdbY->fill(sd_jet.rap(), w);
		
		// if switched to check what lightquarks/gluons pass as a bjet
		//false_positive = 1;
	    }  
        	//count how many times rivet misidentifies 
		outF << false_negative << endl;
		//outF << false_positive << endl;
	}
        
        //done
  
   };
    
    DECLARE_RIVET_PLUGIN(PP2ZB);
}

