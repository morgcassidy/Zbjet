#include <vector>


#include "Pythia8/Pythia.h"

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JadePlugin.hh"

using namespace Pythia8;

using namespace fastjet;

using namespace std;

//-------------------------------------------------------------
// such form of a main function allows to "grep" input from a command line
int main(int argc, char *argv[])
{  
    if (argc != 2)
    {
        std::cout << "Wrong number of argumenst " << argc - 1 << " != 1 is provided!\n";

        std::cout << "You need to type n_events,  yoda_name \n";

        abort();
    }

    int n_events = std::stoi(argv[1]);

    //Set Pythia
    Pythia pythia;
  
    pythia.readString("WeakBosonAndParton:qg2gmZq = on"); // Z/gamma+q production
    pythia.readString("WeakZ0:gmZmode = 2"); //only consider contributions from Z
    
    pythia.readString("HadronLevel:all = off"); // stop process after hard and shower
    pythia.readString("PartonLevel:MPI = off"); // turn off mulit-parton interactions for simplification
  
    // Force leptonic decay of Z ---> mu+ mu-
    pythia.readString("23:onMode = off");
    pythia.readString("23:onIfAny = 13"); 

    pythia.readString("PhaseSpace:pTHatMin = 500;"); 
    pythia.readString("Beams:eCM = 13000");

    pythia.init();

    // Fastjet analysis:
    double Rparam = 0.4;
  
    // Define standard aKT Jets  
    Strategy strategy = Best;
    RecombinationScheme    recombScheme = E_scheme;
    JetDefinition *jetDef = NULL;
    
    jetDef = new JetDefinition(kt_algorithm, Rparam, recombScheme, strategy);

    // Fastjet input
    vector <fastjet::PseudoJet> fjInputs;
    
    // Definition of the JADE algorithm
    JadePlugin jade;
    JetDefinition jade_def = JetDefinition(&jade);

    // Begin event loop. Generate event. Skip if error.
    for (int i = 0; i < n_events; ++i) 
    {
        if (!pythia.next()) continue;

        // Reset Fastjet input
        fjInputs.resize(0);

        for (int j = 0; j < pythia.event.size(); ++j) 
        {
            if (!pythia.event[j].isFinal()) continue;

            if ( pythia.event[j].isParton() )
            {
                double px = pythia.event[j].px();
                double py = pythia.event[j].py();
                double pz = pythia.event[j].pz();
                
                double E = pythia.event[j].e();
                
                fjInputs.push_back( PseudoJet(px, py, pz, E) );
            }
        }
            
        if (fjInputs.size() == 0) 
        {
            cout << "Error: event with no final state particles" << endl;
                
            continue;
        }

        // Run Fastjet algorithm to create aKT jets
        vector <PseudoJet> inclusiveJets, sortedJets;
        ClusterSequence clustSeq(fjInputs, *jetDef);

        inclusiveJets = clustSeq.inclusive_jets();
        sortedJets    = sorted_by_pt(inclusiveJets);
            
        // Recluster leading jet with Jade
        const ClusterSequence jade_cs(sortedJets[0].constituents(), jade_def);
 
        vector<PseudoJet> jade_jets  = jade_cs.inclusive_jets();
        
        if (jade_jets.size() != 1)
        {
            cout << " Something went wrong with the Jade reclustering!\n";
            
            continue;
        }
        
        // This one is a jet we need to recluser into subjets
        PseudoJet jjet = jade_jets[0];

        
        // Reclster into a di-jet
        vector <PseudoJet> di_jet =  jade_cs.exclusive_subjets(jjet, 2);
        
        
        //ClusterSequence::exclusive_subjets (const PseudoJet &jet, int  nsub)
        
        cout << "Event: " << i << "\n";
            
        cout << "Ran " << jade_def.description() << endl;
        cout << "Strategy adopted by FastJet was " << jade_cs.strategy_string() << "\n\n";
        
        cout << "We got " << jade_jets.size() << " Jade jets!\n";
        
        cout << "Primary jet pT = " << jjet.pt() << "\n";
        cout << "We reclustered it into " << di_jet.size() << " subjets!\n";
        cout << "sub jet 1 pt: "    << di_jet[0].pt() << "\n";
        cout << "sub jet 2 pt: "    << di_jet[1].pt() << "\n";
        
        cout <<" pT1 + pT2 = " << di_jet[0].pt() + di_jet[1].pt() << " ? " << jjet.pt() << "\n";
        
        int tmp;
        cin >> tmp;
    }

  // Statistics
  pythia.stat();

  // Done.
  delete jetDef;
 
  return 0;
}
