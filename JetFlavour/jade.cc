#include <vector>


#include "Pythia8/Pythia.h"

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JadePlugin.hh"

#include "fastjet/contrib/Recluster.hh"

using namespace Pythia8;

using namespace fastjet;

using namespace std;

//-------------------------------------------------------------
class Jade
{
public:
    Jade() : _recluster(&_jade, true) 
    {
    }
    
    vector <PseudoJet> split(const PseudoJet &jet, bool if_show = false)
    {
            
        // Recluster leading jet with Jade
        vector<PseudoJet> cnst = jet.constituents();
        
        PseudoJet rec_jade = _recluster(jet);
  
        cout << "Reclustering with: " << rec_jade.description() <<"\n";

        vector<PseudoJet> pieces = rec_jade.pieces();
        
        cout << "   we got " << pieces.size() << " subjets " << endl;
 
        for (int i = 0; i<pieces.size(); ++i) cout << "    " << pieces[i].pt() << "\n";
        
        cout << "\n";
        
        return pieces;
    }
    
private:
    // Oleh: make sure that this is the JetDefinition!
    // Ask Gregory
    JadePlugin _jade;
    
    contrib::Recluster _recluster;

};
//-------------------------------------------------------------
// such form of a main function allows to "grep" input from a command line
int main(int argc, char *argv[])
{  
    if (argc != 2)
    {
        std::cout << "Wrong number of argumenst " << argc - 1 << " != 1 is provided!\n";

        std::cout << "You need to type n_events,  yoda_name \n";

        abort();
    }

    int n_events = std::stoi(argv[1]);

    //Set Pythia
    Pythia pythia;
  
    pythia.readString("WeakBosonAndParton:qg2gmZq = on"); // Z/gamma+q production
    pythia.readString("WeakZ0:gmZmode = 2"); //only consider contributions from Z
    
    pythia.readString("HadronLevel:all = off"); // stop process after hard and shower
    pythia.readString("PartonLevel:MPI = off"); // turn off mulit-parton interactions for simplification
  
    // Force leptonic decay of Z ---> mu+ mu-
    pythia.readString("23:onMode = off");
    pythia.readString("23:onIfAny = 13"); 

    pythia.readString("PhaseSpace:pTHatMin = 500;"); 
    pythia.readString("Beams:eCM = 13000");

    pythia.init();

    // Fastjet analysis:
    double Rparam = 0.4;
  
    // Define standard aKT Jets  
    Strategy strategy = Best;
    RecombinationScheme    recombScheme = E_scheme;
    JetDefinition *jetDef = NULL;
    
    jetDef = new JetDefinition(kt_algorithm, Rparam, recombScheme, strategy);

    // Fastjet input
    vector <fastjet::PseudoJet> fjInputs;
 
    // A helper class to recluster with Jade
    Jade jj;
    
    // Begin event loop. Generate event. Skip if error.
    for (int i = 0; i < n_events; ++i) 
    {
        if (!pythia.next()) continue;

        // Reset Fastjet input
        fjInputs.resize(0);

        for (int j = 0; j < pythia.event.size(); ++j) 
        {
            if (!pythia.event[j].isFinal()) continue;

            if ( pythia.event[j].isParton() )
            {
                double px = pythia.event[j].px();
                double py = pythia.event[j].py();
                double pz = pythia.event[j].pz();
                double E  = pythia.event[j].e();
                
                fjInputs.push_back( PseudoJet(px, py, pz, E) );
            }
        }
            
        if (fjInputs.size() == 0) 
        {
            cout << "Error: event with no final state particles" << endl;
                
            continue;
        }

        // Run Fastjet algorithm to create aKT jets
        vector <PseudoJet> inclusiveJets, sortedJets;
        ClusterSequence clustSeq(fjInputs, *jetDef);

        inclusiveJets = clustSeq.inclusive_jets();
        sortedJets    = sorted_by_pt(inclusiveJets);
            
        // Recluster leading jet with Jade
        vector<PseudoJet> cnst = sortedJets[0].constituents();
 
        PseudoJet j_tmp = sortedJets[0];

        cout << "==================================================================\n";
        cout << "We start declustering\n";
        cout << "Here we follow the hardest branch\n";
        
        while(true)
        {
            vector <PseudoJet> cnst = j_tmp.constituents();
            
            cout << "jet contains " << cnst.size() << " partons\n";
            
            vector<PseudoJet> subjets = jj.split(j_tmp);
            
            if (subjets.size() == 2)
            {
                PseudoJet j1 = subjets[0];
                PseudoJet j2 = subjets[1];
                
                j_tmp = j1;
            }
            else break;
        }

        cout << "End of declustering!\n";
        cout << "==================================================================\n";
 
        cout << "\n\n";
        
        
        int tmp;
        cin >> tmp;
    }

  // Statistics
  pythia.stat();

  // Done.
  delete jetDef;
 
  return 0;
}
