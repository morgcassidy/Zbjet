
###### Monday, 08/01/22
To do's:
- [ ] install software on cluster and migrate files there
- [x] implement a double positive tagger
    - look for pid abs(5) and status 23 
    - turn off Hadronization, turn on MPI
- [x] peep JADE

###### Tuesday, 08/02/22
To do's: 
- [x] spit out count of false negatives
    - have calculated the false negative count. Rivet counts false postives around 5-6% oof the time, with about 95% acceptance. 
- [x] compile and run Jade file
- [x] fix zlib  or ignore and continue
    - some new errors
- will need to cluster anti kts, recluster w jade, then perform sd definition --> Oleh checking with gregory whether easier route by altering parameter

- I've have now also included the qqbar --> Zg process in pythia to pick up those diagram contributions. Now the false negative count is roughly 6%
- have verified selection cuts with Frederico

###### Wednesday, 08/03/22
To do's:



###### Thursday, 08/04/22
To do's:



###### Friday, 08/05/22
To do's:




